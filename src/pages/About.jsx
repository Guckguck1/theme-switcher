import React from 'react';
import { useTheme } from '../context/ThemeContext';

const About = () => {
    const { theme, themeClass } = useTheme();

    return (
        <div className={`max-w-4xl mx-auto mt-10 px-4 ${themeClass} ${theme === 'dark' ? 'bg-gray-900' : 'bg-gray-100'}`}>
            <div className={`p-6 rounded-lg shadow-md ${theme === 'light' ? 'bg-white text-black' : 'bg-gray-800 text-gray-100'}`}>
                <h2 className="text-2xl font-bold mb-4">About Us</h2>
                <p className="text-lg">We've got a button that switches themes faster than a chameleon changes colors. Plus, our footer sticks to the bottom like glue, even in the dark!</p>
                <p className="text-lg">We've been coding through the day and the night, surviving on coffee and code!</p>
                <p className="text-lg">
                    <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ" target="_blank" rel="noopener noreferrer" className="text-blue-500 hover:underline">
                        Check out our awesome new video!
                    </a>
                </p>
            </div>
        </div>
    );
};

export default About;
