import React from 'react';
import { useTheme } from '../context/ThemeContext';

const Contact = () => {
    const { theme, themeClass } = useTheme();

    return (
        <div className={`max-w-4xl mx-auto mt-10 px-4 ${themeClass} ${theme === 'dark' ? 'bg-gray-900' : 'bg-gray-100'}`}>
            <div className={`p-6 rounded-lg shadow-md ${theme === 'light' ? 'bg-white text-black' : 'bg-gray-800 text-gray-100'}`}>
                <h2 className="text-2xl font-bold mb-4">Contact Us</h2>
                <div className="flex flex-col gap-4">
                    <p className="text-lg">For any inquiries or just to say hi:</p>
                    <ul className="list-disc list-inside text-lg">
                        <li>Email: <span className="font-semibold">example@example.com</span></li>
                        <li>Phone: <span className="font-semibold">123-456-7890</span></li>
                        <li>Address: <span className="font-semibold">123 Awesome St, Coding City</span></li>
                    </ul>
                    <p className="text-lg">
                        <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ" target="_blank" rel="noopener noreferrer" className="text-blue-500 hover:underline">
                            Check this out for more information!
                        </a>
                    </p>
                </div>
            </div>
        </div>
    );
};

export default Contact;
