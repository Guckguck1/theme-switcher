import React, { useState } from 'react';
import { useTheme } from '../context/ThemeContext';

const sections = [
    { id: 1, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 2, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 3, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 4, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 5, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 6, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 7, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 8, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 9, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 10, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 11, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 12, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 13, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 14, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 15, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 16, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 17, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 18, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 19, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 20, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 21, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 22, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 23, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
    { id: 24, headline: 'Headline', subheadline: 'Subheadline', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos. Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet minima ullam quia vero facere hic deserunt corrupti laboriosam, iusto libero molestias quidem quas a veritatis incidunt id praesentium quos.' },
];

const Home = () => {
    const { theme } = useTheme();
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [activeSection, setActiveSection] = useState(null);

    const openModal = (section) => {
        setActiveSection(section);
        setIsModalOpen(true);
    };

    const closeModal = () => {
        setIsModalOpen(false);
        setActiveSection(null);
    };

    return (
        <div className={`grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-4 p-4 ${theme === 'dark' ? 'bg-gray-800 text-white' : 'bg-gray-100 text-gray-900'}`}>
            {sections.map(section => (
                <section key={section.id}>
                    <div
                        className={`hover:bg-${theme === 'dark' ? 'gray-700' : 'gray-200'} border border-gray-300 p-2 rounded-lg shadow-lg min-h-[10rem] cursor-pointer`}
                        onClick={() => openModal(section)}
                    >
                        <h1 className="text-xl mb-2">{section.headline}</h1>
                        <h3 className="text-lg mb-2">{section.subheadline}</h3>
                        <p className="text-xs line-clamp-5 overflow-auto">{section.content}</p>
                    </div>
                </section>
            ))}

            {isModalOpen && (
                <div
                    className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50"
                    onClick={closeModal}
                >
                    <div
                        className={`bg-${theme === 'dark' ? 'gray-800' : 'white'} text-${theme === 'dark' ? 'white' : 'black'} p-6 rounded-lg shadow-lg max-w-lg w-full`}
                        onClick={e => e.stopPropagation()}
                    >
                        <h1 className="text-2xl mb-4">{activeSection.headline}</h1>
                        <h3 className="text-xl mb-4">{activeSection.subheadline}</h3>
                        <p className="text-base">{activeSection.content}</p>
                        <button
                            className="mt-4 text-white px-4 py-2 rounded transition-colors duration-300"
                            style={{
                                backgroundColor: theme === 'dark' ? '#4B5563' : '#2563EB',
                            }}
                            onMouseEnter={e => e.target.style.backgroundColor = theme === 'dark' ? '#4C4F59' : '#1E4ED4'}
                            onMouseLeave={e => e.target.style.backgroundColor = theme === 'dark' ? '#4B5563' : '#2563EB'}
                            onClick={closeModal}
                        >
                            Close
                        </button>
                    </div>
                </div>
            )}
        </div>
    );
};

export default Home;