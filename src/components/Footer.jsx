import React from 'react';
import { useTheme } from '../context/ThemeContext';

const Footer = () => {
    const { theme } = useTheme();
    return (
        <footer className={`py-4 sticky bottom-0 ${theme === 'dark' ? 'bg-gray-800 text-white' : 'bg-gray-200 text-black'}`}>
            <div className="flex flex-row justify-around">
                <div>
                    <h3 className="text-sm uppercase font-bold">Resources</h3>
                    <ul className="mt-2">
                        <li><a href="/" className="text-xs hover:underline">Application</a></li>
                        <li><a href="/" className="text-xs hover:underline">Documentation</a></li>
                        <li><a href="/" className="text-xs hover:underline">Systems</a></li>
                        <li><a href="/" className="text-xs hover:underline">FAQ</a></li>
                    </ul>
                </div>
                <div>
                    <h3 className="text-sm uppercase font-bold">Pricing</h3>
                    <ul className="mt-2">
                        <li><a href="/" className="text-xs hover:underline">Overview</a></li>
                        <li><a href="/" className="text-xs hover:underline">Premium Plan</a></li>
                        <li><a href="/" className="text-xs hover:underline">Affiliate</a></li>
                        <li><a href="/" className="text-xs hover:underline">Promotion</a></li>
                    </ul>
                </div>
                <div>
                    <h3 className="text-sm uppercase font-bold">Company</h3>
                    <ul className="mt-2">
                        <li><a href="/" className="text-xs hover:underline">About Us</a></li>
                        <li><a href="/" className="text-xs hover:underline">Blog</a></li>
                        <li><a href="/" className="text-xs hover:underline">Partnership</a></li>
                        <li><a href="/" className="text-xs hover:underline">Press</a></li>
                    </ul>
                </div>
                <div>
                    <h3 className="text-sm uppercase font-bold">Social</h3>
                    <ul className="mt-2">
                        <li><a href="/" className="text-xs hover:underline">Facebook</a></li>
                        <li><a href="/" className="text-xs hover:underline">Twitter</a></li>
                        <li><a href="/" className="text-xs hover:underline">Instagram</a></li>
                        <li><a href="/" className="text-xs hover:underline">LinkedIn</a></li>
                    </ul>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
