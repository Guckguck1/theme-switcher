import React from 'react';
import { useTheme } from '../context/ThemeContext';

const Main = ({ children }) => {
    const { theme } = useTheme();
    return (
        <main className={`p-4 ${theme === 'dark' ? 'bg-gray-900 text-gray-100' : 'bg-gray-100 text-gray-900'} min-h-screen`}>
            {children}
        </main>
    );
};

export default Main;
